# Project Title

 PWM example with a nRF5x chip from Nordic Semiconductor

## Getting Started

Hardware: 

- nRF5x series (nRF52832)
- any device that can be driven with PWM (LED, Piezo...)

Pinout: 

- LED_PIN (nRF52832-P0.20) --> output PIN enabling PWM 

## Installing
- for a quick demo, put this sketch in nRF5_SDK_15.0.0_a53641a\examples\peripheral\\[Your_Project]

## Main Functionality
This firmware creates a PWM channel for output PIN "LED_PIN"

## Built With

Keil V5

## Test
Has been tested with nRF52-DK and custom board 

## Authors

https://bitbucket.org/johnmarcc/


